﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SendGrid;
using SendGrid.Helpers.Mail;
using SendGrid.Helpers.Reliability;
using System;
using System.Threading.Tasks;

namespace TypeFormIntegration
{
    class Program
    {
        const string key = "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";
        const string From = "admin@adfenixleads.com";
        const string FromName = "AdfenixAdmin";

        const string To = "siblee77@gmail.com";
        const string ToName = "AdfenixAdmin";

        static void Main(string[] args)
        {

            var logger = new ConsoleLogger();
            logger.Log(Type.Info, "Application is started...");


            SendEmail().Wait();

            Console.ReadLine();
        }

        static async Task SendEmail()
        {

            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            };

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                DefaultValueHandling = DefaultValueHandling.Ignore,

            };

            var client = new SendGridClient(key);
            //var from = new EmailAddress(From, FromName);

            //var subject = "Test Email from send grid";
            //var to = new EmailAddress(To, ToName);

            //var plainTextContent = "and easy to do anywhere, even with C#";
            //var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            //var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

            //msg.Personalizations = new System.Collections.Generic.List<Personalization>
            //{

            //};

            //var response = await client.SendEmailAsync(msg);


            //var client = new RestClient("https://api.sendgrid.com/v3/mail/send");


            //var client = new RestClient("https://api.sendgrid.com/v3/mail/send");
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("content-type", "application/json");
            //request.AddHeader("authorization", "Bearer <<YOUR_API_KEY_HERE>>");
            //request.AddParameter("application/json", "{\"personalizations\":[{\"to\":[{\"email\":\"john.doe@example.com\",\"name\":\"John Doe\"}],\"dynamic_template_data\":{\"guest\":\"Anthony Wang\",\"partysize\":4,\"english\":true,\"date\":\"April 1st, 2018\"}}],\"from\":{\"email\":\"sam.smith@example.com\",\"name\":\"Sam Smith\"},\"reply_to\":{\"email\":\"sam.smith@example.com\",\"name\":\"Sam Smith\"},\"template_id\":\"d-a6eda312ad2a4e6d876135c0e0d4bdb0\"}", ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);




            //var response = await client.RequestAsync(method: SendGridClient.Method.GET,
            //                                            urlPath: "templates?generations=dynamic");

            //var responseString = await response.Body.ReadAsStringAsync();
            //var templates = JsonConvert.DeserializeObject<TemplateResponse>(responseString, serializerSettings);
            CreateEmailAccount acct = new CreateEmailAccount(new EmailAccount {
                Quota = 500,
                UserName = "nayaaem",
                Password = "123456a"
            });
            acct.Create();

        }

        private static void CreateTypeForm()
        {
            //var task = new Client()
            //     .GetAsync($"{EndPoints.Root}{EndPoints.Account}");
            //task.GetAwaiter().GetResult();

            //var task = new Client()
            //    .GetAsync($"{EndPoints.Root}{EndPoints.Images}");
            //task.GetAwaiter().GetResult();



            //var results = task.GetAwaiter().GetResult();

            //var contractResolver = new DefaultContractResolver
            //{
            //    NamingStrategy = new SnakeCaseNamingStrategy()
            //};

            //var serializerSettings = new JsonSerializerSettings
            //{
            //    ContractResolver = contractResolver
            //};


            //string contents = results.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            //var theme = JsonConvert.DeserializeObject<ThemeResponse>(contents, serializerSettings);


            //var task = new PostClient()
            //    .PostAsync(PostRequests.Images);
            //task.GetAwaiter().GetResult();


            //Get themes
            var logger = new ConsoleLogger();

            Console.Write("  Enter form name - ");
            string name = Console.ReadLine();
            Console.Write("\n");

            Data.FormName = name;
            logger.Log(Type.Info, $"Creating form {Data.FormName}...");

            var getThemestask = new Client()
                .GetAsync($"{EndPoints.Root}{EndPoints.Themes}").GetAwaiter().GetResult();

            var themes = getThemestask.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var themesObjects = JsonConvert.DeserializeObject<ThemeResponse>(themes);

            var id = themesObjects.Items[0].Id;
            var themeHref = $"{EndPoints.Root}{EndPoints.Themes}/{id}";
            Data.Themehref = themeHref;


            //get work space
            var getWorkSpacetask = new Client()
               .GetAsync($"{EndPoints.Root}{EndPoints.WrokSpace}").GetAwaiter().GetResult();

            var workspacesResponse = getWorkSpacetask.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var workspaceObject = JsonConvert.DeserializeObject<WorkSpaceResponse>(workspacesResponse);
            Data.Workspacehref = workspaceObject.items[0].self.href;


            //Get images
            var getImages = new Client()
                .GetAsync($"{EndPoints.Root}{EndPoints.Images}").GetAwaiter().GetResult();

            var getImagesStringResponse = getImages.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var images = JsonConvert.DeserializeObject<ImageResponse[]>(getImagesStringResponse);

            Data.ImagesUrl = images[images.Length - 1].src;




            var createFormTask = new PostClient()
                .PostAsync(PostRequests.Create);

            var createFormResponse = createFormTask.GetAwaiter().GetResult();

            var createForm = JsonConvert.DeserializeObject<CreateFormResponse>(createFormResponse);
            logger.Log(Type.Info, $"Form created !");
            logger.Attatch($"Go to {createForm._links.display} view !");
        }
    }
}
