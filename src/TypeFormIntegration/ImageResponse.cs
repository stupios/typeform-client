﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TypeFormIntegration
{




  

    public class ImageResponse
    {
        public string id { get; set; }
        public string src { get; set; }
        public string file_name { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string media_type { get; set; }
        public bool has_alpha { get; set; }
        public string avg_color { get; set; }
    }


}
