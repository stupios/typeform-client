﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TypeFormIntegration
{
    public class Client
    {
        private ConsoleLogger _logger;

        public Client()
        {
            _logger = new ConsoleLogger();
        }


        public async Task<HttpResponseMessage> GetWithBasicAuthAsync(string uri)
        {
            using (var handler = new HttpClientHandler())
            {
                using (var httpClient = new HttpClient(handler) { BaseAddress = new Uri(uri) })
                {
                    AttatchBasicAuthHeader(httpClient);
                    var req = CreateGetRequest();
                    var response = await SenRequestAsync(uri, httpClient, req);
                    return response;
                }
            }
        }

        public async Task<HttpResponseMessage> GetAsync(string uri)
        {
            using (var handler = new HttpClientHandler())
            {
                using (var httpClient = new HttpClient(handler) { BaseAddress = new Uri(uri) })
                {
                    AttatchAuthHeader(httpClient);
                    var req = CreateGetRequest();
                    var response = await SenRequestAsync(uri, httpClient, req);
                    return response;
                }
            }
        }



        private async Task<HttpResponseMessage> SenRequestAsync(string uri, HttpClient httpClient, HttpRequestMessage req)
        {
            _logger.Log(Type.Info, $"Sendig req to {uri}...");
            var res = await httpClient.SendAsync(req);
            _logger.Log(Type.Info, $"Got response from {uri} {res.ReasonPhrase}");

            return res;
        }

        private static void AttatchAuthHeader(HttpClient httpClient)
            => httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Auth.token);

        private static void AttatchBasicAuthHeader(HttpClient httpClient)
        {
            var byteArray = Encoding.ASCII.GetBytes("adfenixlead:AhLmAnNvIVe8");
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        }

        private static HttpRequestMessage CreateGetRequest()
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get
            };

            return request;
        }
    }
}
