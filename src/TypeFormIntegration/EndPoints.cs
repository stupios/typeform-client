﻿namespace TypeFormIntegration
{
    public static class EndPoints
    {
        public const string Root = "https://api.typeform.com/";
        public const string Account = "me";
        public const string Images = "images";
        public const string Themes = "themes";
        public const string Create = "forms";
        public const string WrokSpace = "workspaces";
    }
}
