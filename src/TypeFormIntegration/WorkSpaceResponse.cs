﻿namespace TypeFormIntegration
{

    public class WorkSpaceResponse
    {
        public Workspaces[] items { get; set; }
        public int page_count { get; set; }
        public int total_items { get; set; }
    }

    public class Workspaces
    {
        public bool _default { get; set; }
        public Forms forms { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public SelfWorkspace self { get; set; }
        public bool shared { get; set; }
    }

    public class Forms
    {
        public int count { get; set; }
        public string href { get; set; }
    }

    public class SelfWorkspace
    {
        public string href { get; set; }
    }



}
