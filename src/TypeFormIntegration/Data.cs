﻿namespace TypeFormIntegration
{
    public static class Data
    {
        public static string Themehref { get; set; }
        public static string Workspacehref { get; set; }
        public static string ImagesUrl { get; set; }
        public static string FormName { get; set; }
    }
}
