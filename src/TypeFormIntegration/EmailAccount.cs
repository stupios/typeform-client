﻿namespace TypeFormIntegration
{
    public class EmailAccount
    {
        public string Domain { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Quota { get; set; }
    }
}
