﻿
$ImageName="console-random-answer-generator"

function Invoke-MSBuild ([string]$MSBuildPath, [string]$MSBuildParameters) {
    Invoke-Expression "$MSBuildPath $MSBuildParameters"
}

function Invoke-Docker-Build ([string]$ImageName, [string]$ImagePath, [string]$DockerBuildArgs = "") {
    echo "docker build -t $ImageName $ImagePath $DockerBuildArgs"
    Invoke-Expression "docker build -t $ImageName $ImagePath $DockerBuildArgs"
}

Invoke-Docker-Build -ImageName $ImageName -ImagePath "."
Invoke-MSBuild -MSBuildPath "MSBuild.exe" -MSBuildParameters ".\TypeFormIntegration.csproj /p:OutputPath=.\publish /p:Configuration=Release"
