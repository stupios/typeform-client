﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TypeFormIntegration
{

    public enum PostRequests
    {
        Images,
        Create
    }
    public class PostClient
    {
        private ConsoleLogger _logger;

        public PostClient()
        {
            _logger = new ConsoleLogger();
        }

        public async Task<string> PostAsync(PostRequests post)
        {
            var uri = $"{EndPoints.Root}{FindEndPoint(post)}";

            using (var handler = new HttpClientHandler())
            {
                using (var httpClient = new HttpClient(handler) { BaseAddress = new Uri(uri) })
                {
                    AttatchAuthHeader(httpClient);
                    var req = CreateRequest(post);
                    var response = await SenRequestAsync(uri, httpClient, req);
                    return response;
                }
            }
        }

        private string FindEndPoint(PostRequests post)
        {
            switch (post)
            {
                case PostRequests.Images:
                    return EndPoints.Images;
                case PostRequests.Create:
                    return EndPoints.Create;
                default:
                    throw new Exception();

            }
        }

        private async Task<string> SenRequestAsync(string uri, HttpClient httpClient, HttpRequestMessage req)
        {
            _logger.Log(Type.Info, $"Sendig req to {uri}...");
            var res = await httpClient.SendAsync(req);
            _logger.Log(Type.Info, $"Got response from {uri} {res.ReasonPhrase}");
            var strings = await res.Content.ReadAsStringAsync();
            //_logger.Attatch("Response content - " + strings);

            return strings;
        }

        private static void AttatchAuthHeader(HttpClient httpClient)
            => httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Auth.token);

        private static HttpRequestMessage CreateRequest(PostRequests post)
        {
            StringContent content = CreateContent(post);

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = content

            };

            return request;
        }

        private static StringContent CreateContent(PostRequests post)
        {

            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            };

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                DefaultValueHandling = DefaultValueHandling.Ignore,

            };


            switch (post)
            {
                case PostRequests.Images:
                    {


                        var ob = new ImageRequest { FileName = "OtherBack.png", Image = ImageConverter.GetBase64StringForImage(@"E:\OtherBack.png") };
                        var contentJson = JsonConvert.SerializeObject(ob, serializerSettings);

                        var content = new StringContent(contentJson, Encoding.UTF8, "application/json");
                        return content;
                    }

                case PostRequests.Create:
                    {
                        var contentsJsons = File.ReadAllText(@"create.json");


                        var createObject = JsonConvert.DeserializeObject<CreateReq>(contentsJsons);

                        createObject.title = Data.FormName;

                        createObject.theme.href = Data.Themehref;

                        createObject.settings.meta.image.href = Data.ImagesUrl;

                        createObject.workspace.href = Data.Workspacehref;
                        createObject.welcome_screens[0].attachment.href = Data.ImagesUrl;


                        foreach (var item in createObject.fields)
                            if (item.attachment.type == "image")
                                item.attachment.href = Data.ImagesUrl;


                        var contentJson = JsonConvert.SerializeObject(createObject, serializerSettings);

                        var content = new StringContent(contentJson, Encoding.UTF8, "application/json");
                        return content;
                    }
                default:
                    throw new Exception();
            }
        }
    }
}
