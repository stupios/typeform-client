﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TypeFormIntegration
{



    public class CreateFormResponse
    {
        public string id { get; set; }
        public string title { get; set; }
        public Theme theme { get; set; }
        public Workspace workspace { get; set; }
        public Settings settings { get; set; }
        public Welcome_Screens[] welcome_screens { get; set; }
        public Thankyou_Screens[] thankyou_screens { get; set; }
        public Field[] fields { get; set; }
        public _Links _links { get; set; }

        public class Theme
        {
            public string href { get; set; }
        }

        public class Workspace
        {
            public string href { get; set; }
        }

        public class Settings
        {
            public bool is_public { get; set; }
            public bool is_trial { get; set; }
            public string language { get; set; }
            public string progress_bar { get; set; }
            public bool show_progress_bar { get; set; }
            public bool show_typeform_branding { get; set; }
            public Meta meta { get; set; }
        }

        public class Meta
        {
            public bool allow_indexing { get; set; }
            public Image image { get; set; }
        }

        public class Image
        {
            public string href { get; set; }
        }

        public class _Links
        {
            public string display { get; set; }
        }

        public class Welcome_Screens
        {
            public string _ref { get; set; }
            public string title { get; set; }
            public Properties properties { get; set; }
            public Attachment attachment { get; set; }
        }

        public class Properties
        {
            public bool show_button { get; set; }
            public string description { get; set; }
            public string button_text { get; set; }
        }

        public class Attachment
        {
            public string type { get; set; }
            public string href { get; set; }
        }

        public class Thankyou_Screens
        {
            public string _ref { get; set; }
            public string title { get; set; }
            public Properties1 properties { get; set; }
            public Attachment1 attachment { get; set; }
        }

        public class Properties1
        {
            public bool show_button { get; set; }
            public bool share_icons { get; set; }
            public string button_mode { get; set; }
            public string button_text { get; set; }
            public string redirect_url { get; set; }
        }

        public class Attachment1
        {
            public string type { get; set; }
            public string href { get; set; }
        }

        public class Field
        {
            public string id { get; set; }
            public string title { get; set; }
            public string _ref { get; set; }
            public Properties2 properties { get; set; }
            public Validations validations { get; set; }
            public Attachment2 attachment { get; set; }
            public string type { get; set; }
        }

        public class Properties2
        {
            public string description { get; set; }
            public string structure { get; set; }
            public string separator { get; set; }
            public bool alphabetical_order { get; set; }
            public Choice[] choices { get; set; }
        }

        public class Choice
        {
            public string label { get; set; }
        }

        public class Validations
        {
            public bool required { get; set; }
        }

        public class Attachment2
        {
            public string type { get; set; }
            public string href { get; set; }
            public float scale { get; set; }
        }
    }





}
