﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TypeFormIntegration
{
    public class ImageConverter
    {        
        public static string GetBase64StringForImage(string imgPath)
        {
            var logger = new ConsoleLogger();
            logger.Log(Type.Info, $"Converting {imgPath} To base 64");
            byte[] imageBytes = System.IO.File.ReadAllBytes(imgPath);
            string base64String = Convert.ToBase64String(imageBytes);
            logger.Attatch($"Conversion complete.");
            return base64String;
        }
    }
}
