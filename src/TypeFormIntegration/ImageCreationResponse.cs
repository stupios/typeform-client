﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TypeFormIntegration
{
    public class ImageCreationResponse
    {
        public string Id { get; set; }
        public string Src { get; set; }
        public string FileName { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string MediaType { get; set; }
        public bool HasAlpha { get; set; }
        public string AvgColor { get; set; }
    }

}
