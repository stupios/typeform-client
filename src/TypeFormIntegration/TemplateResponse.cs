﻿namespace TypeFormIntegration
{
    public class TemplateResponse
    {
        public Template[] Templates { get; set; }
    }

    public class Template
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Generation { get; set; }
        public Version[] Versions { get; set; }
    }

    public class Version
    {
        public string Id { get; set; }
        public string TemplateId { get; set; }
        public int Active { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string UpdatedAt { get; set; }
        public string Editor { get; set; }
    }

}
