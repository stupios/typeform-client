﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TypeFormIntegration
{   
    public class ImageRequest
    {
        //[JsonProperty("image")]
        public string Image { get; set; }

        //[JsonProperty("file_name")]
        public string FileName { get; set; }
    }
}
