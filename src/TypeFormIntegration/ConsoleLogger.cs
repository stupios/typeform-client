﻿using System;

namespace TypeFormIntegration
{

    public enum Type
    {
        Info
    }

    public class ConsoleLogger
    {
        public void Log(Type type, string txt)
        {
            Console.Write("\n  --> ");
            var tag = type.ToString();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(tag);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" - " + txt);
        }
        public void Attatch(string txt)
        {
            Console.Write("\n\t     ");
            Console.Write(txt);
        }
    }
}
