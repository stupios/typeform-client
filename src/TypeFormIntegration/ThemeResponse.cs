﻿namespace TypeFormIntegration
{
    public class ThemeResponse
    {
        public Themes[] Items { get; set; }
        public int TotalItems { get; set; }
        public int PageCount { get; set; }
    }

    public class Themes
    {
        public string Id { get; set; }
        public string Font { get; set; }
        public string Name { get; set; }
        public Colors Colors { get; set; }
        public bool HasTransparentButton { get; set; }
        public string Visibility { get; set; }
        public Background Background { get; set; }
    }

    public class Colors
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Button { get; set; }
        public string Background { get; set; }
    }

    public class Background
    {
        public int Brightness { get; set; }
        public string Layout { get; set; }
        public string Href { get; set; }
    }

}
